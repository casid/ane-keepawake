# README #

ANE-KeepAwake is an Adobe AIR native extension for Android. With this extension you can prevent your app to go into standby mode.

### Why this extension? ###

Basically, because Adobe does not handle SystemIdleMode.KEEP_AWAKE correctly on all Android devices.

According to the official documentation you must set the following permissions on Android

* android.permission.DISABLE_KEYGUARD
* android.permission.WAKE_LOCK

However, there are several flaws with this implementation

* DISABLE KEYGUARD - This disables the lockscreen when you manually put your device into standby mode while the app is opened. This has nothing to do with the intended effect and is simply a security risk.
* When using WAKE_LOCK permission on its own, at least on some Android devices (e.g. Nexus 7), the whole device keeps awake while the app is running in the background!
* WAKE_LOCK never was intended for such a use case. There is a pretty clean and straight forward way in Android to achieve the desired effect without any extra permissions.
* Your users will be glad if there are two permissions less they need to worry about!

### How to use it? ###

After embedding the ANE file (see dist/keepawake.ane) in your project, the use of the extension is pretty straight forward. To keep your app awake you can create a utility method like this:

```
#!actionscript

private function disableSleepMode():void
{
	if (KeepAwake.instance().isSupported)
	{
		KeepAwake.instance().mode = KeepAwake.KEEP_AWAKE;
	}
	else
	{
		NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
	}
}
```

And the following to allow the app to go to sleep again:

```
#!actionscript

private function enableSleepMode():void
{
	if (KeepAwake.instance().isSupported)
	{
		KeepAwake.instance().mode = KeepAwake.NORMAL;
	}
	else
	{
		NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
	}
}
```

