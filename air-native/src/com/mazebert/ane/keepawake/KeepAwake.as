package com.mazebert.ane.keepawake
{
	import flash.external.ExtensionContext;
	
	public class KeepAwake
	{
		public static const NORMAL:String = "NORMAL";
		public static const KEEP_AWAKE:String = "KEEP_AWAKE";
		
		public static function instance():KeepAwake
		{
			if (s_instance == null)
			{
				s_instance = new KeepAwake(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function set mode(mode:String):void
		{
			if (_extensionContext)
			{
				try 
				{
					_extensionContext.call("setMode", mode);
				}
				catch (error:Error)
				{
					trace("Failed to call native method setMode: " + error.message + "(" + error.errorID + ")");
				}
			}
		}
		
		public function get isSupported():Boolean
		{
			return _extensionContext != null;
		}
		
		public function KeepAwake(enforcer:SingletonEnforcer)
		{
			try 
			{
				_extensionContext = ExtensionContext.createExtensionContext("com.mazebert.ane.keepawake", null);
			}
			catch (error:Error)
			{
				trace("Failed to create native extension context: " + error.message + "(" + error.errorID + ")");
			}
		}
		
		private static var s_instance:KeepAwake;		
		private var _extensionContext:ExtensionContext;
	}
}

internal class SingletonEnforcer
{
}