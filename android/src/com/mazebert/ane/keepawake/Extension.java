package com.mazebert.ane.keepawake;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;

public class Extension implements FREExtension {

	@Override
	public FREContext createContext(String contextType) {
		return new ExtensionContext();
	}

	@Override
	public void initialize() {
	}
	
	@Override
	public void dispose() {
	}
	
}
