package com.mazebert.ane.keepawake;

import java.util.HashMap;
import java.util.Map;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.mazebert.ane.keepawake.functions.SetMode;

public class ExtensionContext extends FREContext {

	@Override
	public Map<String, FREFunction> getFunctions() {
		Map<String, FREFunction> functions = new HashMap<String, FREFunction>();
		functions.put("setMode", new SetMode());
		return functions;
	}
	
	@Override
	public void dispose() {
	}
	
}
