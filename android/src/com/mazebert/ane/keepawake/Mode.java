package com.mazebert.ane.keepawake;

import android.util.Log;

import com.adobe.fre.FREInvalidObjectException;
import com.adobe.fre.FREObject;
import com.adobe.fre.FRETypeMismatchException;
import com.adobe.fre.FREWrongThreadException;

public enum Mode {
	NORMAL,
	KEEP_AWAKE;
	
	public static Mode fromString(String mode) {
		if ("NORMAL".equals(mode)) return NORMAL;
		if ("KEEP_AWAKE".equals(mode)) return KEEP_AWAKE;
		
		return null;
	}
	
	public static Mode fromObject(FREObject mode) {
		try {
			return fromString(mode.getAsString());
		} catch (IllegalStateException e) {
			Log.e(LogTag.TAG, e.getMessage(), e);
		} catch (FRETypeMismatchException e) {
			Log.e(LogTag.TAG, e.getMessage(), e);
		} catch (FREInvalidObjectException e) {
			Log.e(LogTag.TAG, e.getMessage(), e);
		} catch (FREWrongThreadException e) {
			Log.e(LogTag.TAG, e.getMessage(), e);
		}
		return null;
	}
}
