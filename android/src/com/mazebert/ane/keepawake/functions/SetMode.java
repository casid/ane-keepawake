package com.mazebert.ane.keepawake.functions;

import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.mazebert.ane.keepawake.LogTag;
import com.mazebert.ane.keepawake.Mode;

public class SetMode implements FREFunction {

	@Override
	public FREObject call(FREContext context, FREObject[] arguments) {
		if (arguments.length == 1) {
			Mode mode = Mode.fromObject(arguments[0]);
			setMode(context, mode);
		}
		return null;
	}
	
	private void setMode(FREContext context, Mode mode) {
		if (mode == null) {
			Log.e(LogTag.TAG, "Unable to set mode: null");
			return;
		}
		
		Window window = context.getActivity().getWindow();
		if (window == null) {
			Log.e(LogTag.TAG, "Unable to set mode: Window of activity could not be retrieved.");
			return;
		}
		
		switch (mode) {
		case NORMAL:
			window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			break;
		case KEEP_AWAKE:
			window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			break;
		}
	}
	
}
