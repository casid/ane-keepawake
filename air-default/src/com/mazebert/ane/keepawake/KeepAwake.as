package com.mazebert.ane.keepawake
{
	/**
	 * Interface stub for default usage.
	 */
	public class KeepAwake
	{
		public static const NORMAL:String = "NORMAL";
		public static const KEEP_AWAKE:String = "KEEP_AWAKE";
		
		public static function instance():KeepAwake
		{
			if (s_instance == null)
			{
				s_instance = new KeepAwake(new SingletonEnforcer());
			}
			return s_instance;
		}
		
		public function set mode(mode:String):void
		{
		}
		
		public function get isSupported():Boolean
		{
			return false;
		}
		
		public function KeepAwake(enforcer:SingletonEnforcer)
		{
		}
		
		private static var s_instance:KeepAwake;
	}
}

internal class SingletonEnforcer
{
}